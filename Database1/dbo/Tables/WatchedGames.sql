﻿CREATE TABLE [dbo].[WatchedGames] (
    [ID]        INT             IDENTITY (1, 1) NOT NULL,
    [URL]       NVARCHAR (1024) NULL,
    [Email]     NVARCHAR (1024) NULL,
    [EmailSent] BIT             NOT NULL,
    CONSTRAINT [PK_dbo.WatchedGames] PRIMARY KEY CLUSTERED ([ID] ASC)
);

