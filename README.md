# README #

### What is this repository for? ###

* Parse a wikipedia page for a release date, send an email on that day that says "hey this game was released".
* Version 1

### How do I get set up? ###

You'll need a database set up according to the database project. Run a schema compare and apply the changes.

You'll need to make a file called ConnectionStrings.config. Place it next to Web.config. It should look something like this:

```
#!XML

<connectionStrings>
  <add name="DefaultConnection" connectionString="server=yourserver;user id=yourusername;password=yourpassword;database=yourdatabase" providerName="System.Data.SqlClient" />
</connectionStrings>
```

You'll need to make a file called AppSettings.config. Place it next to Web.config. It should look something like this:
```
#!XML
<appSettings>
  <add key="webpages:Version" value="3.0.0.0" />
  <add key="webpages:Enabled" value="false" />
  <add key="ClientValidationEnabled" value="true" />
  <add key="UnobtrusiveJavaScriptEnabled" value="true" />
  <add key="EmailFrom" value="you@gmail.com"/>
  <add key="EmailHost" value="smtp.gmail.com"/>
  <add key="EmailPassword" value="password"/>
  <add key="EmailPort" value="587"/>
</appSettings>
```