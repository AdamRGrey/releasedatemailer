﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ReleaseDateMailer.Startup))]
namespace ReleaseDateMailer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
