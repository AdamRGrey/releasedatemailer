﻿using System;

namespace ReleaseDateMailer.Services
{
    public interface IPageParserService
    {
        DateTime? GetReleaseDate(string responseBody);
        string GetGameName(string responseBody);
    }
}
