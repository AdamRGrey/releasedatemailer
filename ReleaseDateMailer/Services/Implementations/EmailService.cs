﻿using ReleaseDateMailer.Models;
using System.Configuration;
using System.Net.Mail;

namespace ReleaseDateMailer.Services.Implementations
{
    public class EmailService : IEmailService
    {
        private SmtpClient _smtpClient;
        protected SmtpClient smtpClient
        {
            get
            {
                if(_smtpClient == null)
                {
                    var port = 0;
                    if(!int.TryParse(ConfigurationManager.AppSettings["EmailPort"], out port))
                    {
                        throw new ConfigurationErrorsException("EmailPort is not an integer!");
                    }
                    _smtpClient = new SmtpClient(ConfigurationManager.AppSettings["EmailHost"], port);
                    _smtpClient.UseDefaultCredentials = false;
                    _smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["EmailPassword"]);
                    _smtpClient.EnableSsl = true;
                }
                return _smtpClient;
            }
        }
        public void SendEmail(WatchedGame watchedGame, string gameName)
        {
            var from = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]);
            var to = new MailAddress(watchedGame.Email);
            var message = new MailMessage(from, to);
            message.Body = gameName + " is out as of today!";
            smtpClient.Send(message);
        }
    }
}