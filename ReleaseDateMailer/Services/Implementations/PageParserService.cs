﻿using HtmlAgilityPack;
using System;
using System.Linq;

namespace ReleaseDateMailer.Services.Implementations
{
    public class PageParserService : IPageParserService
    {
        public DateTime? GetReleaseDate(string responseBody)
        {
            var AcceptedRegions = new string[] { "WW", "NA" };
            var doc = new HtmlDocument();
            doc.LoadHtml(responseBody);
            foreach (var table in doc.DocumentNode.Descendants("table"))
            {
                foreach (var body in table.Descendants("tbody"))
                {
                    foreach(var tr in body.Descendants("tr"))
                    {
                        foreach(var th in tr.Descendants("th"))
                        {
                            if(th.InnerText == "Release&nbsp;date(s)" || th.InnerText == "Release date(s)")
                            {
                                foreach(var ul in tr.Descendants("ul"))
                                {
                                    foreach (var li in ul.Descendants("li"))
                                    {
                                        var region = li.InnerText.Substring(0, 2);
                                        if(AcceptedRegions.Contains(region))
                                        {
                                            var datestring = li.InnerText.Substring(3);
                                            var date = new DateTime();
                                            if (DateTime.TryParse(datestring, out date))
                                            {
                                                return date;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        public string GetGameName(string responseBody)
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(responseBody);
            foreach (var table in doc.DocumentNode.Descendants("table"))
            {
                foreach (var body in table.Descendants("tbody"))
                {
                    foreach (var tr in body.Descendants("tr"))
                    {
                        foreach (var th in tr.Descendants("th"))
                        {
                            if (th.InnerText == "Developer(s)")
                            {
                                return body.Descendants("tr").First().Descendants("th").First().InnerText;
                            }
                        }
                    }
                }
            }
            return null;
        }
    }
}