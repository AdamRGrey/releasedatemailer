﻿using ReleaseDateMailer.DataAccess;
using ReleaseDateMailer.DataAccess.Repositories;
using ReleaseDateMailer.Models;
using System;
using System.Net.Http;

namespace ReleaseDateMailer.Services.Implementations
{
    public class GameWatcherService : IGameWatcherService
    {
        private readonly IWatchedGameRepository _watchedGameRepository;
        private HttpClient _client;
        protected HttpClient client
        {
            get { return _client ?? (_client = new HttpClient()); }
        }
        private readonly IPageParserService _pageParserService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmailService _emailService;

        public GameWatcherService(IPageParserService pageParserService, IWatchedGameRepository watchedGameRepository, IUnitOfWork unitOfWork, IEmailService emailService)
        {
            _pageParserService = pageParserService;
            _watchedGameRepository = watchedGameRepository;
            _unitOfWork = unitOfWork;
            _emailService = emailService;
        }

        public void Monitor(WatchedGame watchedGame)
        {
            if (watchedGame.EmailSent)
                return;

            try
            {
                var responseBody = client.GetStringAsync(watchedGame.URL).Result;
                var releaseDate = _pageParserService.GetReleaseDate(responseBody);
                if(releaseDate != null)
                {
                    if(releaseDate.Value - DateTime.Today < TimeSpan.Zero)
                    {
                        var gameName = _pageParserService.GetGameName(responseBody);
                        
                        _emailService.SendEmail(watchedGame, gameName);

                        watchedGame.EmailSent = true;
                        _watchedGameRepository.Update(watchedGame);
                        _unitOfWork.Commit();
                    }
                }
            }
            catch (HttpRequestException hre)
            {
                Console.WriteLine($"HttpRequestException! {hre.Message}");
            }
        }
    }
}