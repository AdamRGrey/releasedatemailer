﻿using ReleaseDateMailer.Models;

namespace ReleaseDateMailer.Services
{
    public interface IGameWatcherService
    {
        void Monitor(WatchedGame watchedGame);
    }
}
