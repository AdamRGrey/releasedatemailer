﻿using ReleaseDateMailer.Models;

namespace ReleaseDateMailer.Services
{
    public interface IEmailService
    {
        void SendEmail(WatchedGame watchedGame, string gameName);
    }
}
