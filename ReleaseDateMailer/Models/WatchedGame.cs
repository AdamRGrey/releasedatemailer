﻿using System.ComponentModel.DataAnnotations;

namespace ReleaseDateMailer.Models
{
    public class WatchedGame
    {
        public int ID { get; set; }
        [MaxLength(1024)]
        public string URL { get; set; }
        [MaxLength(1024)]
        public string Email { get; set; }
        public bool EmailSent { get; set; }
    }
}