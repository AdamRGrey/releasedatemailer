﻿using Autofac;
using Autofac.Integration.WebApi;
using ReleaseDateMailer.Controllers;
using ReleaseDateMailer.DataAccess;
using ReleaseDateMailer.DataAccess.Implementations;
using ReleaseDateMailer.DataAccess.Repositories;
using ReleaseDateMailer.Services;
using ReleaseDateMailer.Services.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ReleaseDateMailer.App_Start
{
    public static class IoCContainer
    {
        public static void Config()
        {
            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;

            RegisterDataAccess(builder);

            RegisterControllers(builder);

            RegisterServices(builder);

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static void RegisterDataAccess(ContainerBuilder builder)
        {
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterType<WatchedGameRepository>().As<IWatchedGameRepository>().InstancePerRequest();
        }

        private static void RegisterControllers(ContainerBuilder builder)
        {
            builder.RegisterType<UpdateController>().InstancePerRequest();
        }

        private static void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterType<GameWatcherService>().As<IGameWatcherService>().InstancePerRequest();
            builder.RegisterType<PageParserService>().As<IPageParserService>().InstancePerRequest();
            builder.RegisterType<ReleaseDateMailer.Services.Implementations.EmailService>().As<IEmailService>().InstancePerRequest();
        }
    }
}