namespace ReleaseDateMailer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class maxlength_on_email : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.WatchedGameModels", "Email", c => c.String(maxLength: 1024));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.WatchedGameModels", "Email", c => c.String());
        }
    }
}
