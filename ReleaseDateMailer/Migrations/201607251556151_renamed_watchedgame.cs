namespace ReleaseDateMailer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamed_watchedgame : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.WatchedGameModels", newName: "WatchedGames");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.WatchedGames", newName: "WatchedGameModels");
        }
    }
}
