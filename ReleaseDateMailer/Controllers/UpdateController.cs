﻿using ReleaseDateMailer.DataAccess;
using ReleaseDateMailer.DataAccess.Repositories;
using ReleaseDateMailer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ReleaseDateMailer.Controllers
{
    public class UpdateController : ApiController
    {
        IWatchedGameRepository _watchedGameRepository;
        IGameWatcherService _gameWatcherService;

        public UpdateController(IWatchedGameRepository watchedGameRepository, IGameWatcherService gameWatcherService)
        {
            _watchedGameRepository = watchedGameRepository;
            _gameWatcherService = gameWatcherService;
        }
        [HttpGet]
        public string Index()
        {
            var watchedGames = _watchedGameRepository.GetAll();
            var toReturn =  $"{watchedGames.Count()} watched game(s).";
            
            foreach (var wg in watchedGames)
            {
                toReturn += $"\nparsing {wg.URL} (emailsent = {wg.EmailSent}).";
                _gameWatcherService.Monitor(wg);
            }

            return toReturn;
        }
    }
}
