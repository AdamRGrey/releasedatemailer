﻿using ReleaseDateMailer.DataAccess;
using ReleaseDateMailer.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ReleaseDateMailer.Controllers
{
    [Authorize]
    public class WatchedGameController : Controller
    {
        private ReleaseDateMailerDBContext db = new ReleaseDateMailerDBContext();

        // GET: WatchedGame
        public ActionResult Index()
        {
            return View(db.WatchedGameSet.ToList());
        }

        // GET: WatchedGame/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WatchedGame watchedGame = db.WatchedGameSet.Find(id);
            if (watchedGame == null)
            {
                return HttpNotFound();
            }
            return View(watchedGame);
        }

        // GET: WatchedGame/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WatchedGame/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,URL")] WatchedGame watchedGame)
        {
            if (ModelState.IsValid)
            {
                watchedGame.Email = User.Identity.Name;
                db.WatchedGameSet.Add(watchedGame);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(watchedGame);
        }

        // GET: WatchedGame/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WatchedGame watchedGame = db.WatchedGameSet.Find(id);
            if (watchedGame == null)
            {
                return HttpNotFound();
            }
            return View(watchedGame);
        }

        // POST: WatchedGame/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,URL")] WatchedGame fromForm)
        {
            if (ModelState.IsValid)
            {
                var fromDB = db.WatchedGameSet.Find(fromForm.ID);
                if (fromDB == null)
                {
                    return HttpNotFound();
                }
                fromDB.URL = fromForm.URL;
                db.Entry(fromDB).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fromForm);
        }

        // GET: WatchedGame/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WatchedGame watchedGame = db.WatchedGameSet.Find(id);
            if (watchedGame == null)
            {
                return HttpNotFound();
            }
            return View(watchedGame);
        }

        // POST: WatchedGame/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WatchedGame watchedGame = db.WatchedGameSet.Find(id);
            db.WatchedGameSet.Remove(watchedGame);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
