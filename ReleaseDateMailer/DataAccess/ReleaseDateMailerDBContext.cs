﻿using Microsoft.AspNet.Identity.EntityFramework;
using ReleaseDateMailer.Models;
using System.Data.Entity;

namespace ReleaseDateMailer.DataAccess
{
    public class ReleaseDateMailerDBContext : IdentityDbContext<ApplicationUser>
    {
        public ReleaseDateMailerDBContext() : base("DefaultConnection") { }

        public DbSet<WatchedGame> WatchedGameSet { get; set; }

        public static ReleaseDateMailerDBContext Create()
        {
            return new ReleaseDateMailerDBContext();
        }
    }
}