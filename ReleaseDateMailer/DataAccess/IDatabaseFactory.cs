﻿using System.Data.Entity;

namespace ReleaseDateMailer.DataAccess
{
    public interface IDatabaseFactory
    {
        DbContext Get();
    }
}
