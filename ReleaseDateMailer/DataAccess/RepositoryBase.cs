﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ReleaseDateMailer.DataAccess
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        private DbContext _dataContext;
        private readonly DbSet<T> _dbset;
        protected IDatabaseFactory databaseFactory
        {
            get;
            private set;
        }
        protected DbContext dataContext
        {
            get { return _dataContext ?? (_dataContext = databaseFactory.Get()); }
        }
        protected RepositoryBase(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
            _dbset = dataContext.Set<T>();
        }

        public virtual T Add(T entity)
        {
            return _dbset.Add(entity);
        }

        public virtual void AddRange(IEnumerable<T> entities)
        {
            var saveSetting = dataContext.Configuration.AutoDetectChangesEnabled;
            dataContext.Configuration.AutoDetectChangesEnabled = false;

            try
            {
                _dbset.AddRange(entities);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dataContext.Configuration.AutoDetectChangesEnabled = saveSetting;
            }
        }

        public virtual void Update(T entity)
        {
            _dbset.Attach(entity);
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        public virtual void Delete(Expression<Func<T, bool>> where)
        {
            IEnumerable<T> objects = _dbset.Where(where).AsEnumerable();
            foreach (T obj in objects)
            {
                _dbset.Remove(obj);
            }
        }

        public virtual void Delete(long id)
        {
            var entity = _dbset.Find(id);
            _dbset.Remove(entity);
        }

        public virtual T GetById(long id)
        {
            return _dbset.Find(id);
        }

        public virtual T Get(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where).FirstOrDefault();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return _dbset.ToList();
        }

        public virtual IEnumerable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where).ToList();
        }
    }
}