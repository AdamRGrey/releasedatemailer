﻿namespace ReleaseDateMailer.DataAccess
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
