﻿using ReleaseDateMailer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReleaseDateMailer.DataAccess.Repositories
{
    public interface IWatchedGameRepository : IRepository<WatchedGame>
    {
    }
}
