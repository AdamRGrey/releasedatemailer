﻿using System.Data.Entity;

namespace ReleaseDateMailer.DataAccess.Implementations
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private ReleaseDateMailerDBContext DatabaseContext;

        public DbContext Get()
        {
            return DatabaseContext ?? (DatabaseContext = new ReleaseDateMailerDBContext());
        }
    }
}