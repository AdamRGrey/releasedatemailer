﻿using ReleaseDateMailer.DataAccess.Repositories;
using ReleaseDateMailer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReleaseDateMailer.DataAccess.Implementations
{
    public class WatchedGameRepository : RepositoryBase<WatchedGame>, IWatchedGameRepository
    {
        public WatchedGameRepository(IDatabaseFactory databaseFactory) : base(databaseFactory)
        {
        }
    }
}